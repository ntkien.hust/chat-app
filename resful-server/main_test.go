package main

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestHandleGetLogin(t *testing.T) {
	mux := http.NewServeMux()
	mux.HandleFunc("/", HandleGetLogin)

	writer := httptest.NewRecorder()
	request, _ := http.NewRequest("GET", "/", nil)
	mux.ServeHTTP(writer, request)

	if writer.Code != 200 {
		t.Errorf("Response code is %v", writer.Code)
	}
}

func TestHandleGetRegister(t *testing.T) {
	mux := http.NewServeMux()
	mux.HandleFunc("/register", HandleGetRegister)

	writer := httptest.NewRecorder()
	request, _ := http.NewRequest("GET", "/register", nil)
	mux.ServeHTTP(writer, request)

	if writer.Code != 200 {
		t.Errorf("Response code is %v", writer.Code)
	}
}

func TestHandleLogin(t *testing.T) {
	mux := http.NewServeMux()
	payload := strings.NewReader("{\"username\": \"lam\",\"passwords\": \"3026972\"}")
	mux.HandleFunc("/login", HandleLogin)

	writer := httptest.NewRecorder()
	request, _ := http.NewRequest("POST", "/login", payload)
	request.Header.Add("cache-control", "no-cache")
	request.Header.Add("Content-Type", "application/json")
	mux.ServeHTTP(writer, request)

	if writer.Code != 200 {
		t.Errorf("Response code is %v", writer.Code)
	}
}

func TestHandleRegister(t *testing.T) {
	mux := http.NewServeMux()
	payload := strings.NewReader("{\"id\":123,\"username\":\"kienzcv\",\"passwords\":\"12345\",\"firstname\":\"kjzxvkjas\",\"lastname\":\"lkjbzxlv\"}")	
	mux.HandleFunc("/register", HandleRegister)

	writer := httptest.NewRecorder()
	request, _ := http.NewRequest("POST", "/register", payload)
	request.Header.Add("cache-control", "no-cache")
	request.Header.Add("Content-Type", "application/json")
	mux.ServeHTTP(writer, request)

	if writer.Code != 200 {
		t.Errorf("Response code is %v", writer.Code)
	}
}


