package userdata

import (
	"testing"
	types "resful-server/datastruct"

)

func TestSelectPasswords(t *testing.T) {
	testcase1 := SelectPasswords("kien")
	if testcase1.Passwords == "" {
		t.Errorf("Output expect is: %v", testcase1)
	}

	testcase2 := SelectPasswords("!@#$#$%")
	if testcase2.Username != "" {
		t.Errorf("Output is unexpected: %v", testcase2)
	}

	testcase3 := SelectPasswords("")
	if testcase2.Username != "" {
		t.Errorf("Output is unexpected: %v", testcase3)
	}
}

func TestInsertUser(t *testing.T) {
	user := types.User{ID: 0, Username: "kienafa", Passwords: "asdfvczx", Firstname: "asdfcvz", Lastname: "adsfcxvad"}
	testcase1 := InsertUser(user)
	if testcase1 != "Register successfully"{
		t.Errorf("Output is unexpected: %s", testcase1)
	}

	user.Username = ""
	testcase2 := InsertUser(user)
	if testcase2 != "Username is empty!"{
		t.Errorf("Output is unexpected: %s", testcase2)
	}
}